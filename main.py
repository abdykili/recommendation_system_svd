import pandas as pd
import numpy as np
from sklearn.decomposition import TruncatedSVD
from sklearn.cluster import KMeans
from flask import Flask, request, jsonify
import psycopg2

app = Flask(__name__)

# Define an endpoint to generate movie recommendations for a user using a combined approach
@app.route('/recommendations/<int:user_id>', methods=['GET'])
def combined_recommendations(user_id):
    #Connect to the PostgreSQL database
    conn = psycopg2.connect(
        host="localhost",
        database="recommendations-db",
        user="admin",
        password="admin")

    # Read the movie ratings data from the database
    ratings_df = pd.read_sql_query('SELECT * FROM movie_ratings', conn)

    # Create a user-item matrix from the ratings data
    ratings_matrix = ratings_df.pivot(index='user_id', columns='movie_id', values='rating').fillna(0)

    # Perform Singular Value Decomposition (SVD) on the ratings matrix
    SVD = TruncatedSVD(n_components=20, random_state=17)
    svd_matrix = SVD.fit_transform(ratings_matrix)

    # Perform K-Means clustering on the SVD matrix
    kmeans = KMeans(n_clusters=3, random_state=17).fit(svd_matrix)

    # Create a Flask application instance
    app = Flask(__name__)

    # Use SVD to generate additional movie recommendations for the user
    movie_recommendations = []
    if user_id in ratings_matrix.index:
        # Get the user's ratings
        user_ratings = ratings_matrix.loc[user_id, :]
        # Use SVD and clustering to determine the user's cluster
        user_cluster = kmeans.predict(SVD.transform(user_ratings.values.reshape(1, -1)))[0]
        # Get all users in the same cluster as the current user
        cluster_users = np.where(kmeans.labels_ == user_cluster)[0]
        # Calculate the average ratings for each movie in the cluster
        cluster_ratings = ratings_matrix.iloc[cluster_users].mean(axis=0)
        # Sort the movies by their average ratings, in descending order
        cluster_ratings = cluster_ratings.sort_values(ascending=False)
        # Get the top recommended movies that the user hasn't rated yet
        rec_movies = cluster_ratings.index[~cluster_ratings.index.isin(user_ratings.dropna().index)]
        # Use SVD to generate additional movie recommendations for the user
        svd_movie_recommendations = []
        for movie_id in user_ratings.index:
            if user_ratings[movie_id] == 0:
                # Get the index of the movie in the ratings_matrix
                movie_idx = ratings_matrix.columns.get_loc(movie_id)
                # Calculate the correlation between the current movie and all movies in the user's cluster
                corr_movie = np.corrcoef(svd_matrix[:, movie_idx], svd_matrix[cluster_users, :])[0, 1:]
                # Sort the movies by their correlation with the current movie, in descending order
                rec_movies_idx = np.argsort(corr_movie)[::-1]
                # Remove NaN values
                rec_movies_idx = rec_movies_idx[~np.isnan(corr_movie[rec_movies_idx])]
                # Get the top recommended movies
                rec_movies_idx = rec_movies_idx[:10]
                # Add the movie recommendations to the list
                svd_movie_recommendations += ratings_matrix.columns[rec_movies_idx].tolist()
        # Combine the two lists of movie recommendations and remove duplicates
        movie_recommendations = list(set(svd_movie_recommendations + rec_movies[:10].tolist()))
        return jsonify({'movie_recommendations': movie_recommendations})
    else:
        return jsonify({'message': 'User ID not found.'}), 404

# Run the Flask application
if __name__ == '__main__':
    app.run(debug=True)